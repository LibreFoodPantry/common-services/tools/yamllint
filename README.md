# yamllint

[yamllint](https://github.com/adrienverge/yamllint) is a linter for YAML files.

To enable yamllint in your project's pipeline add `yamllint` to the
`ENABLE_JOBS` variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "yamllint"
```

The `ENABLE_JOBS` variable is a comma-separated string value, e.g.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install the [YAML extension](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml)
for Visual Studio Code.

## Using Locally in Your Development Environment

yamllint can be run locally in a linux-based bash shell or in a linting shell
script with this Docker command:

```bash
docker run --rm -v "${PWD}":/app/project -w /app/project\
    registry.gitlab.com/pipeline-components/yamllint:latest \
    yamllint .
```

## Configuration

You can [configure](https://github.com/adrienverge/yamllint#features) yamllint
to define new rules, ignore files, disable checks on certain lines or blocks by
using a configuration file at the top level of the project
(often called .yamllint.yaml). You can also disable checks on lines or blocks
with comments in the file.

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
